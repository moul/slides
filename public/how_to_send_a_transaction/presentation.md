name:begin
class: center, middle

# How to send a transaction

RML13 − 23th May 2019 − Perpignan − Moul

<img src="../images/silkaj_logo.svg" width=30 /> <img src="../images/duniterpy_logo.png" width=30 /> <img src="../images/g1_logo.svg" width=30 /> <img src="../images/duniter_logo.svg" width=30 /> <img src="../images/dunitrust_logo.svg" width=30 /> <img src="../images/cesium_logo.svg" width=30 />

---
### Presentation
--

- Moul, https://moul.re
--

- Joined Duniter project around 2014−5
--

- Silkaj author and developer: CLI client

.center[<img src="../images/silkaj_logo.svg" width=60 />]

--

- DuniterPy Developer: Python library for Duniter clients

.center[<img src="../images/duniterpy_logo.png" width=60 />]

---
### Outline
--

- Documents
???
- Presentation of Duniter documents

--

- Source management
???
- Present: How sources of money is managed in Duniter
--

- Transaction
???
- Transaction document in details
--

- Signature
???
- How can we unsure that the document have well been issued by its author?
--

- Study cases
???
- Practical studies on different cases we could encounter while sending a tx

---

### Glossary
--

- tx: Transaction
--

- ud: Universal Dividend
--

- wot: Web of Trust

---
### Duniter documents
--

- A crypto-currency usually implements the block and the transaction documents

--

- Duniter implements four additional WoT documents:
  - certification, identity, membership, and revocation

--

- Five documents sent by a Duniter client:
  - transaction
  - certification
  - identity
  - membership
  - recovation

???
- Not of our interest, but there is also the smart contract document, implemented by Ethereum

- Focus on the transaction document
  - Most used document
  - Most complex document

---
name: source
### Money source management in Duniter

???
Before talking about the transaction, we have to understand how sources of money works in Duniter

--
        ______
       |      |
       | +UD  |  block                     "dividend": 1007,
       |______|

???
- All sources are created through an UD

--



        inputs
         \ /
        ______
       |      |
       |  Tx  |
       |______|
         \ /
        outputs

???
- Inputs and outputs of sources

---
template: source

--

`curl <host>:<port>/tx/sources/<pubkey>`
--

```json
"type": "T",
"noffset": 0,
"identifier": "5E1713DA1401D43FDBD0CD0AB430F91A054D56B34C32B0DDEA7560BDE02221BC",
"amount": 100000,
"base": 0,
"conditions": "SIG(Tu2Ch53QxEvA7yfo5VDUt6vL7grqQ3asuT6vGEcG3sw4k)"
```
--

- `type`: A source can either come from an UD (`D`) or a TX (`T`)
--

- ud:
  - `noffset`: the block number
  - `identifier`: the public key of the creator
???
- Identify the source of this source
--

- tx:
  - `noffset`: position among the outputs
  - `identifier`: hash of the tx
--

- `amount`: source amount
--

- `base`: decimal base of the amount
???
- Why do we specify a base here?
  - This is to handle big numbers.
  - As creating of lots of money
  - As the databases are limited by the size limit of integer.
  - We implemented a rotation of the numbers
--

- `conditions`: conditions to unlock the source

---

### Transaction
--
name: tx

```bash
Version: 10
Type: Transaction
Currency: g1-test
Blockstamp: 364442-0004F5855ACC76545A0C2170407107FBB202AC8F601BF71FCD4F746DE12FF4F5
Locktime: 0
Issuers: WULdRTxspGdJzrs4vpZsWLGWsu37DjqoHyhGDFr5amh
Inputs: 5550:0:T:45FEF6B2D5BE516E308B68CFF5E263BCA366B110B82B096C29AA6177FD53CDF3:0
Unlocks:
0:SIG(0)
Outputs:
5550:0:SIG(5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH)
Comment: Tx RML13
9iyE4ENArPpJRAAOunb7K1iqUHeANkxxY9kgryv62VP6npzqSVLExhPerL1K9bage6Yj8lZ47kmAOBkGQLjUBA==
```
--

- `Version`: version of the document
???
Made for evolutions
--

- `Type`: Transaction, Certification,…
???
Public: Can someone tell me what are the three others documents?
--

- `Currency`: currency name: g1, g1-test
--

- `Blockstamp`: block number + hash of the block in which the tx have be added
--

- `Locktime`: delay Δt before the tx could be included
???
Feature not implemented by any client today

---
template: tx

- `Issuers`: list of pubkeys’ issuers
???
Yeah, there could be several issuers of a single transaction
--

- `Inputs` sources: `amount:base:type:identifier:noffset`
--

- `Unlocks`: conditions to unlock inputs: `input position:` + `SIG(0)` or `XHX(password)`
<!--
https://git.duniter.org/nodes/typescript/duniter/blob/dev/doc/Protocol.md#locking-and-unlocking-functions
-->
--

- `Outputs` sources: `amount:base:condition`
--

- `Comment`: transaction reference

--
- signatures: encryptions of the document’s hash from the issuers’ private keys

---

### Output lock conditions
--

- `SIG`: `pubkey`: SIGnature from a pubkey allowed to free the source
--

- `CLTV`: `CheckLockTimeVerify`: absolute time: `timestamp`:
  - source unlocked when this absolute timestamp has been reached
--

- `CSV`: `CheckSequenceVerify`: relative time: `time`:
  - source unlocked when the specified time has been elapsed since the tx have been sent
--

- `XHX`: `X` Hash of password: `sha_hash`:
  - Allows to unlock a source with a password
  - Allows cross-currency tx.
--

- This four conditions can be compiled together with operators:
  - and: `&&`
  - or: `||`
  - `(`, `)`, and space
--

- There is a limit of 1.000 characters
<!--
https://git.duniter.org/nodes/typescript/duniter/blob/dev/doc/Protocol.md#input-condition
https://en.bitcoin.it/wiki/Timelock
-->

---
#### Inputs’ unlocks and outputs’ locks conditions
--

- To unlock `CLTV` and `CSV`, is by waiting, they do not exits as input unlocks
--

- SIG and XHX output conditions and Input unlocks are working in pairs
--

- If an output have been locked with a certain SIG, it can only be unlocked with this SIG:
  - tx1: output condition: SIG(pubkey_recipient)
  - tx2: input unlock: `0:SIG(0)` + signature of the recipient
--

- If an output have been locked with a XHX, it has to be unlocked with a XHX and the good password:
  - hash = SHA256(password)
  - tx1: output condition: XHX(hash)
  - tx2 input unlock: XHX(password)

---
#### Signature: How documents truthfullness get verified
<!--
Source: https://commons.wikimedia.org/wiki/File:Digital_Signature_diagram.svg
https://medium.com/@seandotau/digital-signatures-and-the-blockchain-716a6bc3c25e
-->
???
- How can we unsure that the document have well been issued by its author?
- What if someone could send a transaction from your wallet, a certification from your member account?
--

![](../images/Digital_Signature_diagram.svg)
???
- Signing process: document content + signatures: hash(content)
- The node compare the document and the signature hash thanks to the public key
- Hash: Scrypt
- Encryption/Decryption, Signature: libsodium
- Same mechanism for all cryptocurrencies and for all Duniter’s documents.
---

class: center, middle

## Study practical cases
???
Let’s jump to practical examples

---

#### How to send a transaction if the pubkey only has a bigger source?
???
What if we have a big source and we want to sent a transaction of a certain amount. How could you proceed?
--

```bash
Version: 10
Type: Transaction
Currency: g1-test
Blockstamp: 364442-0004F5855ACC76545A0C2170407107FBB202AC8F601BF71FCD4F746DE12FF4F5
Locktime: 0
Issuers: WULdRTxspGdJzrs4vpZsWLGWsu37DjqoHyhGDFr5amh
Inputs: 5550:1:T:45FEF6B2D5BE516E308B68CFF5E263BCA366B110B82B096C29AA6177FD53CDF3:0
20000:1:T:481D471F8D6065ACB911EDE504C07776D185396E77C98538F1D672C5DFCB271F:0
Unlocks:
0:SIG(0)
1:SIG(0)
Outputs:
5945:1:SIG(5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH)
19605:1:SIG(WULdRTxspGdJzrs4vpZsWLGWsu37DjqoHyhGDFr5amh)
Comment: Tx RML13
9iyE4ENArPpJRAAOunb7K1iqUHeANkxxY9kgryv62VP6npzqSVLExhPerL1K9bage6Yj8lZ47kmAOBkGQLjUBA==
```

???
Return back change to the issuer
- Has some might some might have noticed, previous transaction was simplified.
- And the source was the exact amount we wanted.
- That’s pretty rare. So, a usual tx looks like this one.

---

#### How to send Ğ1 10.000?
--

- Transactions are limited to 40 inputs
--

- So, if you do not manage to combine the amount with this 40 sources
--

- You’ll have to compine thoses sources into one.
--

- This is what is called a change operation
--

- The programm could chain several intermediaries txs

---

#### How to send a single transaction to many recipients
--

- Transactions generated by Remuniter and Silkaj to remunerate blocks’ issuers and Duniter contributors
--

```json
"outputs": [
  "20140:0:SIG(2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ)",
  "20140:0:SIG(FEkbc4BfJukSWnCU6Hed6dgwwTuPFTVdgz5LpL4iHr9J)",
  "20140:0:SIG(D9D2zaJoWYWveii1JRYLVK3J4Z7ZH3QczoKrnQeiM6mx)",
  "20140:0:SIG(38MEAZN68Pz1DTvT3tqgxx4yQP6snJCQhPqEFxbDk4aE)",
  "20140:0:SIG(5cnvo5bmR8QbtyNVnkDXWq6n5My6oNLd1o6auJApGCsv)",
  "20140:0:SIG(GfKERHnJTYzKhKUma5h1uWhetbA8yHKymhVH2raf2aCP)",
  "20140:0:SIG(7F6oyFQywURCACWZZGtG97Girh9EL1kg2WBwftEZxDoJ)",
  "20140:0:SIG(HQKzoCALqqkAZhZHXGMvDjvGuceKdGjtb8MpMYJnoxvJ)",
  "20140:0:SIG(CRBxCJrTA6tmHsgt9cQh9SHcCc8w8q95YTp38CPHx2Uk)",
  "20140:0:SIG(2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT)",
  "20140:0:SIG(4FgeWzpWDQ2Vp38wJa2PfShLLKXyFGRLwAHA44koEhQj)",
  "20140:0:SIG(55oM6F9ZE2MGi642GGjhCzHhdDdWwU6KchTjPzW7g3bp)",
  "20140:0:SIG(BH8ZqCsp4sbHeDPPHpto53ukLLA4oMy4fXC5JpLZtB2f)",
  "20140:0:SIG(45GfjkWCWQhJ3epJVGC2NSg1Rcu4Ue1vDD3kk9eLs5TQ)",
  "20140:0:SIG(77UVGVmbBLyh5gM51X8tbMtQSvnMwps2toB67qHn32aC)",
  "20140:0:SIG(Do99s6wQR2JLfhirPdpAERSjNbmjjECzGxHNJMiNKT3P)",
  "32332:0:SIG(78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8)"
],
```

???
multi-output tx

---

#### How to send a transaction with unlockable sources at a certain time

--

- CSV(time):
--

CSV(time.now() + 48h)

--

- CLTV(timestamp):
--

CLTV(48h)

---

#### Password locking with XHX
--

- tx1:
```python
password = "rml13"
hash = SHA256(password)
output_condition = "XHX(hash)"
```
--

- tx2:
```python
input_unlock = "XHX(password)"
```
--

- Node:
```python
SHA256(password) = hash
```
???
Check that password hashed passed in the input unlock equals the hash of the source

---

#### How to send a transaction from several issuers
???
- Let’s imagine you want to send a big transaction from two wallets
- Otherwise you would not have enough money for this tx
- The client should implement a way to sign twice the document by asking twice the authentication
--

- inputs from several issuers
--

- signature from several issuers

---
#### Cross-currencies exchange
--

- Locktime: 48h
--

- `(XHX(SHA256(password)) && SIG(recipient)) || (SIG(issuer) && CLTV(48h))`

<!--
https://duniter.org/en/transactions-0-2-overview/
https://duniter.org/fr/transactions-0-2-overview/
-->

---

### [Silkaj v0.7.0 release](https://forum.duniter.org/t/silkaj-v0-7-0-release-duniterpy/6099)
???
Released this week-end
--

- Migration to DuniterPy and Click
--

- Display Txs history
--

- Debian, and Ubuntu packages
--

.center[<img src="../images/silkaj_logo.svg" width=150 />]

---

name: links
#### Links
##### General
- Technical Forum: [forum.duniter.org](https://forum.duniter.org)
- XMPP chatroom [chat.duniter.org](https://chat.duniter.org)
    - [xmpp:duniter@muc.duniter.org](xmpp:duniter@muc.duniter.org)
- Matrix chatroom: `#duniter:matrix.org`
- Duniter forge: https://git.duniter.org
--


##### Dig deeper
- DuniterPy examples: Send transaction:
  - https://git.duniter.org/clients/python/duniterpy/blob/dev/examples/send_transaction.py
- Duniter Protocol: https://git.duniter.org/nodes/typescript/duniter/blob/dev/doc/Protocol.md
- Cross-currencies txs article: https://duniter.org/en/transactions-0-2-overview/

---

#### Credit
- Slides published under the GNU GPL v3 free license

- Slide source: [git.duniter.org/moul/slides](https://git.duniter.org/moul/slides)
- Slide Pages: [moul.duniter.io/slides/how_to_send_a_tx](https://moul.duniter.io/slides/how_to_send_a_transaction/)
- Slideshow created using [remark](https://github.com/gnab/remark)
--


#### Donation
- Public key as a QRcode:
<img src="../images/qrcode.png" width=100 />

- My public key: GfKERHnJTYzKhKUma5h1uWhetbA8yHKymhVH2raf2aCP
???
If you did appreciate the presentation, you can donate here

---
class: center, middle

## Thanks
???
Thank you for your attention

---

class: center, middle
## Questions?
(En français si vous le souhaitez)
???
If you have a question, this is the right moment

---

name: end
class: center, middle
.center[<img src="../images/g1_logo.svg" width=500 />]
