name:begin
class: center, middle

# Silkaj evolution

![](../images/silkaj_logo.svg)

---
### Outline

- What is Silkaj?
- Numbers
- Evolution since v0.3.0
 - Installation eased with pip
 - Transaction (multi-output, better UX)
 - Certification
 - wot command
 - logo
- Demo
- Future
 - DuniterPy
 - GVA
 - Wot documents issuance
- Questions

---

### What is Silkaj?

- Silkaj is a powerfull and lightweight command line client.

- Published under the libre license: GNU APGL v3

- Written in Python, it allows communicating with nodes implementing Duniter protocol.

- It can communicate with Ğ1 and ĞTest currencies, and others

- It allows sending transactions, certifications, and consulting the blockchain.

- Four authentication methods: Scrypt, File, Seed, WIF.

- Communication through the BMA API.

---

### Numbers

- 327 commits

- 2 years

- 6 contributors

- 1.739 lines of code of Python

---

### History
Since last conference:

- v0.3.0 was released for the nineth LCM in Le Havre

- Since then, versions 0.4.0, 0.5.0 and 0.6.0 have been released.

---

### Transaction

- Multi-output transactions

- Check there is enough money on the pubkey

- Transaction resume before the transaction

 - amount before and after the transaction

 - show pubkeys and ids of out and in-comming recipients

---

#### Transaction multi-output
```bash
silkaj tx --auth-file --amount 2 --gtest --output \
DpJse2t7fyH9LC9FTMQHsMGZToXLmVQ8EV2eP47ipHDC:7KL2QXXFULDpsQY4UdSr5oEVx6rFE6oxeagRdkCX35bf
╒═══════════════════════════╤══════════════════════════════════════════════╕
│ pubkey’s amount before tx │ 15375.33 ĞTest                               │
├───────────────────────────┼──────────────────────────────────────────────┤
│ tx amount (unit)          │ 4.0 ĞTest                                    │
├───────────────────────────┼──────────────────────────────────────────────┤
│ tx amount (relative)      │ 0.0104 UD ĞTest                              │
├───────────────────────────┼──────────────────────────────────────────────┤
│ pubkey’s amount after tx  │ 15371.33 ĞTest                               │
├───────────────────────────┼──────────────────────────────────────────────┤
│ from (pubkey)             │ 5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH │
├───────────────────────────┼──────────────────────────────────────────────┤
│ from (id)                 │ moul-test                                    │
├───────────────────────────┼──────────────────────────────────────────────┤
│ to (pubkey)               │ DpJse2t7fyH9LC9FTMQHsMGZToXLmVQ8EV2eP47ipHDC │
├───────────────────────────┼──────────────────────────────────────────────┤
│ to (id)                   │ vit                                          │
├───────────────────────────┼──────────────────────────────────────────────┤
│ to (pubkey)               │ 7KL2QXXFULDpsQY4UdSr5oEVx6rFE6oxeagRdkCX35bf │
├───────────────────────────┼──────────────────────────────────────────────┤
│ to (id)                   │ cuckooland                                   │
╘═══════════════════════════╧══════════════════════════════════════════════╛
Do you confirm sending this transaction? [yes/no]: yes
Generate Transaction:
   - From:    5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH
   - To:      DpJse2t7fyH9LC9FTMQHsMGZToXLmVQ8EV2eP47ipHDC
   - To:      7KL2QXXFULDpsQY4UdSr5oEVx6rFE6oxeagRdkCX35bf
   - Amount:  4.0
Transaction successfully sent.
```
---

### Certification

New command to certify identities:

```bash
silkaj cert jytou --auth-file --gtest
In which language would you like to display Ğ1 license [en/fr]? fr
Do you approve Ğ1 license [yes/no]? yes
╒════════╤══════════════════════════════════════════════╤════╤══════════════════════════════════════════════╕
│ Cert   │ From                                         │ –> │ To                                           │
├────────┼──────────────────────────────────────────────┼────┼──────────────────────────────────────────────┤
│ ID     │ moul-test                                    │ –> │ jytou                                        │
├────────┼──────────────────────────────────────────────┼────┼──────────────────────────────────────────────┤
│ Pubkey │ 5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH │ –> │ 2pyPsXM8UCB88jP2NRM4rUHxb63qm89JMEWbpoRrhyDK │
╘════════╧══════════════════════════════════════════════╧════╧══════════════════════════════════════════════╛
Do you confirm sending this certification? [yes/no]: yes
Certification successfully sent.
```

---

### wot

- Certifications received and sent of an identity
- Expiration date of the certifications
- Membership informations:
 - Membership expiration due to certification or membership document expiration

---

### wot

```bash
silkaj wot moul-test --gtest
moul-test (5B8iM…) from block #167750-0000A51F…
received 8 and sent 15/100 certifications:
|  received_expire  |      received       |       sent        |  sent_expire  |
|-------------------+---------------------+-------------------+---------------|
|    2018-11-21     |      esprit ✔       |     MeluaTest     |  2018-11-19   |
|    2018-12-15     |      GAS2000 ✔      |      esprit       |  2018-11-21   |
|    2018-12-19     | matograine-G1Test ✔ |      GAS2000      |  2018-12-10   |
|    2018-12-20     |   scanlegentil ✔    |       guwop       |  2018-12-10   |
|    2019-01-29     |        vit ✔        | matograine-G1Test |  2019-02-04   |
|    2019-02-07     |       Elois ✔       |       Elois       |  2019-02-07   |
|    2019-02-15     |    cuckooland ✔     |    Vincentest     |  2019-02-07   |
|    2019-02-16     |     piaaf31GT ✔     |    cuckooland     |  2019-02-07   |
|                   |                     |        vit        |  2019-02-07   |
|                   |                     |   scanlegentil    |  2019-02-07   |
|                   |                     |       cgeek       |  2019-02-07   |
|                   |                     |     isawien45     |  2019-02-07   |
|                   |                     |     aguy-dev      |  2019-02-07   |
|                   |                     |     piaaf31GT     |  2019-02-14   |
|                   |                     |     kimamila      |  2019-02-15   |

Membership expiration due to certification expirations: 2018-12-20
member: True
Membership document expiration: 2018-12-31
Sentry: True
outdistanced: False
```

---
class: center, middle

### Logo

![](../images/silkaj_logo.svg)

Logo designed by Attilax

---

### Demo

- silkaj amount RML12butzV3xZmkWnNAmRwuepKPYvzQ4euHwhHhzvSY:78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8:TENGx7WtzFsTXwnbrPEvb6odX2WnqYcnnrjiiLvp1mS

As for Remuniter:
- silkaj diffi
- silkaj issuers 0

---

### Future
- Configuration
- Testing structure
- Transactions list for a pubkey
- DuniterPy: refactor to use this API
- GVA: Future client API using GraphQL
- Allow sending identity, membership and revocation documents

---

name:links
### Links

- silkaj website: https://silkaj.duniter.org
- silkaj forge: https://git.duniter.org/clients/python/silkaj

- Technical Forum: [forum.duniter.org](https://forum.duniter.org)
- XMPP chatroom [chat.duniter.org](https://chat.duniter.org)
    - [xmpp:duniter@muc.duniter.org](xmpp:duniter@muc.duniter.org)

---

### Credit
- Slides published under libre license GNU GPLv3

- Slide source: [git.duniter.org/moul/slides](https://git.duniter.org/moul/slides)
- Slide Pages: https://moul.duniter.io/slides/silkaj_evolution/

### Donation
![Public key as a QRcode](../images/qrcode.png)
- My public key: GfKERHnJTYzKhKUma5h1uWhetbA8yHKymhVH2raf2aCP
- Duniter developers pubkey: 78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8

---
class: center, middle

## Thanks

---

class: center, middle
## Questions?
(En français si vous le souhaitez)

---
name: end
class: center, middle
![Ğ1 logo](../images/g1_logo.svg)

