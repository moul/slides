name:begin
class: center, middle

# How to contribute to Silkaj

\#RML14 − 28th November 2019 − Toulouse − Moul

<img src="../images/silkaj_logo.svg" width=50 />
<img src="../images/duniterpy_logo.png" width=50 />
<img src="../images/deathreaper.svg" width=50 />
<img src="../images/Python-logo-notext.svg" width=50 />
<img src="../images/poetry-logo.svg" width=30 />
<img src="../images/GitLab_Logo.svg" width=50 />
<img src="../images/g1_logo.svg" width=50 />

Slides: [frama.link/TdU0VkUy](https://frama.link/TdU0VkUy)

---
### Outline
--

- Contributions since the LCM13
--

- GitLab
--

- How to contribute to Silkaj
--

  - Projects presentation
--

  - Install the development environment with Poetry
--

  - Ticket tracker system
--

  - Code architecture
--

  - Repository architecture, files
--

  - Continuous Integration and Delivery (CI/CD)
--

  - Dive into source files: `cert`

---
name: contrib

### Contributions since the LCM13
--

#### Silkaj
--

- Migrate the devevelopment environment tool:
  - from Pipenv to Poetry
  - Dependency manager, virtual environment
--

- Create Docker images with Poetry installed for all supported Python versions (3.5, 3.6, 3.7, 3.8)
--

- Set-up the CI/CD: checks, tests, PyPI releases 
--

- Mentoring matograine
--

- Sarted to wrote tests: coverage 37 −> 51%, mocking
--

- Big improvements on the `info` command
---

template: contrib

```bash
silkaj info --money (--wot, --blockchain)
╒═══════════════════════════╤══════════════════════════════════════════╕
│         Currency          │                    Ğ1                    │
├───────────────────────────┼──────────────────────────────────────────┤
│     Connected to node     │            g1.duniter.org:443            │
├───────────────────────────┼──────────────────────────────────────────┤
│       Target growth       │                9.76%/year                │
├───────────────────────────┼──────────────────────────────────────────┤
│      Current growth       │                0.01%/year                │
├───────────────────────────┼──────────────────────────────────────────┤
│ Monetary mass per member  │               4,753.66 Ğ1                │
├───────────────────────────┼──────────────────────────────────────────┤
│       Monetary mass       │             11,370,753.12 Ğ1             │
├───────────────────────────┼──────────────────────────────────────────┤
│           Δt UD           │                  a day                   │
├───────────────────────────┼──────────────────────────────────────────┤
│       Next UD date        │           2019-11-24 12:00:00            │
├───────────────────────────┼──────────────────────────────────────────┤
│    Δt UD reevaluation     │                 183 days                 │
├───────────────────────────┼──────────────────────────────────────────┤
│     Previous UD value     │ 10.07 Ğ1 (from 2019-03-22 to 2019-09-20) │
├───────────────────────────┼──────────────────────────────────────────┤
│     Current UD value      │     10.11 Ğ1 (since 2019-09-20 16h)      │
├───────────────────────────┼──────────────────────────────────────────┤
│   Next UD reevaluation    │     10.15 Ğ1 (from  2020-03-21 06h)      │
├───────────────────────────┼──────────────────────────────────────────┤
│      Next UD formula      │    UD(t+1) = UD(t) + c² × M(t)/N(t+1)    │
├───────────────────────────┼──────────────────────────────────────────┤
│ Current decimal unit base │               0 (10^0 = 1)               │
╘═══════════════════════════╧══════════════════════════════════════════╛
```
---

template: contrib

##### DeathReaper
.center[<img src="../images/deathreaper.svg" width=100 />]
--

- `silkaj excluded`
- Bot which notify of the forums excluded identities from the WoT

--

```md
### Exclusions de la toile de confiance Ğ1, pertes des statuts de membre
> Message automatique. Merci de notifier vos proches de leur exclusion de la toile de confiance

- [`Williambenhaim`](https://g1.duniter.fr/#/app/block/274234/7D4334A459D6F35A089492D346B07C6711AFB0E8273CE9DCE3BC65C594061A25?ssl=true) :
  - **certifié·e par** Jonelio108, Gauri, Djtina12.

- [Gil alias `DoMi5500`](https://g1.duniter.fr/#/app/block/274249/807A1D4D378BABA6F53E50F1FDD448EFF3F5D098428DA4D5D6A73DC8A18EDE06?ssl=true) :
  - **certifié·e par** Eauvive, nicoleC, VivienProuvot, LucieHaget, @Pol, ArnaudFleuri.
```

---

template: contrib

#### DuniterPy
--

- Agreement between vit and me
--

- Dev Env: Poetry, CI/CD
--

- Improvements, Bug fixes
--


#### GitLab
--

- Backups
- Upgrades
- Migration

---

### GitLab
.center[<img src="../images/GitLab_logo_text.svg" width=200 />]
--

- https://git.duniter.org
--

- Ticket tracker, git repository
--

- Forge to support developers of projects around the Ğ1
--

- To support this service, you can donate Ğ1 to [78Zz pubkey](https://duniter.org/fr/financements/)
--

- This money will be used to pay the server hosting and the system adminstration
--

- Ask for an account on the forum
--

- Nice feature: CI/CD
--

- Two runners

---
class: center, middle

## How to contribute to Silkaj
---

#### Projects
--

##### What’s the Silkaj Project about?
.center[<img src="../images/silkaj_logo.svg" width=100 />]
--

- Powerfull, lightweight, and multi-platform command line client written with Python for Ğ1 and Ğ1-Test currencies
--

- Project is aiming at creating a generic tool to manage accounts and wallets, and to monitor the currency

--

##### DuniterPy
.center[<img src="../images/duniterpy_logo.png" width=100 />]

- Python APIs library to implement Ğ1 clients software

---

#### Install the development environment with Poetry
<center><img src="../images/poetry-logo.svg" width=50 /></center>
--

- [Documentation to install Silkaj with Poetry](https://git.duniter.org/clients/python/silkaj/blob/dev/doc/install_poetry.md)
--

- On Debian Buster:
```bash
sudo apt install libsodium23 python3-pip python3-venv
python3 -m pip install poetry --user
```
--
```bash
git clone https://git.duniter.org/clients/python/silkaj.git
cd silkaj
poetry install
```
--
```bash
poetry run silkaj
```
--
```bash
poetry run pytest
```
---
name: tts
#### Ticket tracker system
--

- Issues
- Merge requests
- Milestones
- Releases
---

template: tts
![](../images/silkaj_0.8_milestone.png)

---
#### Code architecture
--

```bash
User -> TUI -> Silkaj -> DuniterPy -> Network >-\
User <- TUI <- Silkaj <- DuniterPy <- Network <_/
```
--

##### **TUI**: terminal user interface
--

- [Click](https://click.palletsprojects.com/): Command line interface creation kit
--

- Display: `tabulate`, `texttable`, `click.echo()`, `print()`
--


##### Network APIs
--

- [BMA](https://git.duniter.org/nodes/typescript/duniter/blob/dev/doc/HTTP_API.md): *Basic Merkle API*, historic ~~s2s~~ and s2c
--

- [Cesium+](https://git.duniter.org/clients/cesium-grp/cesium-plus-pod): BMA and Elasticsearch based, improved for Cesium
--

- [WS2P](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0004_ws2p_v1.md): *WebSocket To Peer*, s2s, node API based on websocket
--

- [GVA](https://git.duniter.org/nodes/common/doc/blob/graphql_api_rfc/rfc/0003%20RFC%20GraphQL%20API%20for%20Duniter%20Clients.md): *GraphQL Verification API*, future client API

.center[<img src="../images/GraphQL_Logo.svg" width=50 />]

---

name: files
#### Repository architecture, files
--

- [`pyproject.toml`](https://git.duniter.org/clients/python/silkaj/blob/dev/pyproject.toml), `poetry.lock`
--


```toml
[tool.poetry]
name = "silkaj"
version = "0.7.3"
description = "Powerfull, lightweight, and multi-platform command line client written with Python for Ğ1 and Ğ1-Test currencies"
authors = ["Moul <moul@moul.re>"]
maintainers = ["Moul <moul@moul.re>"]
readme = "README.md"
license = "AGPL-3.0-or-later"
homepage =  "https://silkaj.duniter.org"
repository = "https://git.duniter.org/clients/python/silkaj"
documentation = "https://git.duniter.org/clients/python/silkaj/tree/dev/doc"
keywords = ["g1", "duniter", "cryptocurrency", "librecurrency", "RTM"]

[tool.poetry.dependencies]
python = "^3.5.3"
duniterpy = "^0.55.1"
click = "^7.0"
tabulate = "^0.8.3"
PyNaCl = "^1.3"

[tool.poetry.dev-dependencies]
pytest = "^5.1"
(…)
```
---
template: files

- `release.sh`
--

- `tests/test_*.py`
--

- `silkaj/*.py`
--

:
  - 15 −> 18 files

---
template: files

- [`.gitlab-ci.yml`](https://git.duniter.org/clients/python/silkaj/blob/dev/.gitlab-ci.yml)
--


```yml
stages:
  - checks
  - tests
  - publish
  - coverage

variables:
  DOCKER_IMAGE: "registry.duniter.org/docker/python3/poetry"
  PYTHON_VERSION: "3.7"

image: $DOCKER_IMAGE/$PYTHON_VERSION:latest

.changes:
  only:
    changes:
      - silkaj/*.py
      - tests/*.py
```
---
template: files
- [`.gitlab-ci.yml`](https://git.duniter.org/clients/python/silkaj/blob/dev/.gitlab-ci.yml)

```yml
.tests:
  extends: .changes
  stage: tests
  image: $DOCKER_IMAGE/$PYTHON_VERSION:latest
  script:
    - poetry install
    - poetry run pytest
(…)
tests-3.8:
  extends: .tests
  tags: [poetry-78]
  variables:
    PYTHON_VERSION: "3.8"
(…)
pypi:
  stage: publish
  only: [tags]
  when: manual
  script:
    - poetry publish --build --username $PYPI_LOGIN --password $PYPI_PASSWORD
```
---

name: cicd
#### Continuous Integration and Delivery (CI/CD)
--

##### Silkaj and DuniterPy pipelines
--

![Silkaj pipeline](../images/silkaj_pipeline.png)
--

- Checks: build, [`Black` format](https://black.readthedocs.io/en/stable/)
--

- Tests with [`pytest`](https://pytest.org) framework on all supported Python versions
--

- Release on the *Python Package Index* [PyPI](https://pypi.org/project/silkaj/) and [PyPI test](https://pypi.org/project/silkaj/)
--

- [Coverage report](https://clients.duniter.io/python/silkaj/index.html) deployed on GitLab Pages
--

![DuniterPy pipeline](../images/duniterpy_pipeline.png)

---

template: cicd
name: docker
##### Docker images
--

- Repository: https://git.duniter.org/docker/python3/poetry/
--


###### Dockerfile:

```docker
FROM python:3.8-slim-buster

# Install libsodium
RUN apt update && \
apt install --yes libsodium23 black make && \
rm -rf /var/lib/apt/lists

# Install Poetry
RUN pip3 install poetry --pre
```
--

- Standard [Python Docker images](https://github.com/docker-library/python) for all the supported versions
--

- Install [Poetry](https://poetry.eustace.io/) on top
---
template: docker

###### .gitlab-ci.yml

```yml
(…)
script:
  - docker login -u "gitlab-ci-token" -p "$CI_BUILD_TOKEN" $CI_REGISTRY
  - docker build --pull -t "$CI_REGISTRY_IMAGE/$PYTHON_VERSION:$CI_BUILD_TAG" $PYTHON_VERSION
  - docker login -u "gitlab-ci-token" -p "$CI_BUILD_TOKEN" $CI_REGISTRY
  - docker tag "$CI_REGISTRY_IMAGE/$PYTHON_VERSION:$CI_BUILD_TAG" "$CI_REGISTRY_IMAGE/$PYTHON_VERSION:$CI_BUILD_TAG"
  - docker push "$CI_REGISTRY_IMAGE/$PYTHON_VERSION:$CI_BUILD_TAG"
  - docker tag "$CI_REGISTRY_IMAGE/$PYTHON_VERSION:$CI_BUILD_TAG" "$CI_REGISTRY_IMAGE/$PYTHON_VERSION:latest"
  - docker push "$CI_REGISTRY_IMAGE/$PYTHON_VERSION:latest"
(…)
```
--

- Build from the `Dockerfile`
--

- Publish the image to the registry with:
--

  - the git tag
  - with the `latest` tag
---

template: docker
##### Pipeline

![](../images/poetry_pipeline.png)

---
template: docker
##### Registry

![](../images/poetry_registry.png)

---
name: cert
#### Dive into source files for the `cert` command
--

- [`pyproject.toml`](https://git.duniter.org/clients/python/silkaj/blob/dev/pyproject.toml):

```toml
[tool.poetry.scripts]
silkaj = "silkaj.cli:cli"
```
--

- [`silkaj/cli.py`](https://git.duniter.org/clients/python/silkaj/blob/dev/silkaj/cli.py):

```python
from click import group, help_option, version_option, option, pass_context
from silkaj.cert import send_certification

@group()
@help_option("-h", "--help")
@version_option(SILKAJ_VERSION, "-v", "--version")
@option("--gtest", "-gt", is_flag=True, help="ĞTest: `https://g1-test.duniter.org` endpoint")
@pass_context
def cli(ctx, peer, gtest, auth_scrypt, nrp, auth_file, file, auth_seed, auth_wif):
    ctx.obj = dict()
    ctx.ensure_object(dict)
    ctx.obj["GTEST"] = gtest
    …

cli.add_command(send_certification)
```
---

template: cert
- [`silkaj/silkaj/cert.py`](https://git.duniter.org/clients/python/silkaj/blob/dev/silkaj/cert.py):

```python
# imports

@command("cert", help="Send certification")
@argument("id_to_certify")
@coroutine
async def send_certification(id_to_certify):
    client = ClientInstance().client
    id_to_certify = await get_informations_for_identity(id_to_certify)

    # Authentication
    key = auth_method()

    # 3 Checks (…)
    if issuer_pubkey == id_to_certify["pubkey"]:
        message_exit("You can’t certify yourself!")

    # Display license
    head = await HeadBlock().head_block
    currency = head["currency"]
    license_approval(currency)

    # Certification confirmation
    await certification_confirmation(
        issuer, issuer_pubkey, id_to_certify, main_id_to_certify
    )
```

---
template: cert

```python
    identity = Identity(
        version=10,
        currency=currency,
        pubkey=id_to_certify["pubkey"],
        uid=main_id_to_certify["uid"],
        ts=block_uid(main_id_to_certify["meta"]["timestamp"]),
        signature=main_id_to_certify["self"],
    )

    certification = Certification(
        version=10,
        currency=currency,
        pubkey_from=issuer_pubkey,
        identity=identity,
        timestamp=BlockUID(head["number"], head["hash"]),
        signature="",
    )

    # Sign document
    certification.sign([key])

    # Send certification document
    response = await client(wot.certify, certification.signed_raw())

    if response.status == 200:
        print("Certification successfully sent.")
    else:
        print("Error while publishing certification: {0}".format(await response.text()))

    await client.close()
```
---

template: cert

##### DuniterPy example
- [`duniterpy/examples/send_certification.py`](https://git.duniter.org/clients/python/duniterpy/blob/dev/examples/send_certification.py)

---
class: center, middle
--
# END

---
#### Nice features comming
--

- Send multiple outputs transactions with different amounts
--

- Send membership document
--

- Add ability to unlock and lock transaction with `XHX`, `CSV`, `CLTV` conditions

---

name: links
#### Links
--

##### Communication means
- Technical Forum: [forum.duniter.org](https://forum.duniter.org)
- XMPP chatroom [chat.duniter.org](https://chat.duniter.org)
    - [xmpp:duniter@muc.duniter.org](xmpp:duniter@muc.duniter.org)
- Matrix chatroom: `#duniter:matrix.org`
--


##### Silkaj
- Silkaj website: https://silkaj.duniter.org/
- Silkaj repository: https://git.duniter.org/clients/python/silkaj
- [CONTRIBUTING.md](https://git.duniter.org/clients/python/silkaj/blob/dev/CONTRIBUTING.md)
--


##### DuniterPy
- DuniterPy repository: https://git.duniter.org/clients/python/duniterpy
---

#### Credit
- Slides published under the GNU GPL v3 free license

- Slides source: [git.duniter.org/moul/slides](https://git.duniter.org/moul/slides)
- Slides Pages: [moul.duniter.io/slides/how_to_contribute_to_silkaj](https://moul.duniter.io/slides/how_to_contribute_to_silkaj/)
- Slideshow created using [remark](https://github.com/gnab/remark)
--


#### Donation
- My public key: GfKERHnJTYzKhKUma5h1uWhetbA8yHKymhVH2raf2aCP

- Or, as a QRcode:
<img src="../images/qrcode.png" width=100 />
???
If you did appreciate the presentation, you can donate here

---
class: center, middle

## Thanks
???
Thank you for your attention

---

class: center, middle
## Questions?
(En français si vous le souhaitez)
???
If you have a question, this is the right moment

---

name: end
class: center, middle
.center[<img src="../images/g1_logo.svg" width=500 />]
