name:begin
class: center, middle

# Contributing to Silkaj

\#RML16 − 28th May 2022 − L’Éstachon (Sisteron) − Moul

<img src="../images/silkaj_logo.svg" width=50 />
<img src="../images/Python-logo-notext.svg" width=45 />
<img src="../images/poetry-logo.svg" width=30 />
<img src="../images/pre-commit_logo.svg" width=50 />
<img src="../images/gitlab-icon.png" width=50 />
<img src="../images/gtk_logo.svg" width=40 />
<img src="../images/substrate_logo.png" width=55 />
<img src="../images/duniter_logo.svg" width=50 />
<img src="../images/g1_logo.svg" width=50 />

---

This presentation is an augmented presentation of:
- [How to contribute to Silkaj](https://git.duniter.org/moul/slides#rml14-toulouse-28th-november-2019)
- RML14 − Toulouse − 28th November 2019

Call for contributors

---
### Outline
--

- Pre-requisites
--

- Project goals
--

- Set-up the development environment
  - Poetry
  - Pre-commit
--

- Pytest
--

- Continuous Integration and Delivery (CI/CD)
--

- Future developments
  - Substrate APIs usage
  - Graphical interface

---

#### Pre-requisites
- `git` basics
- GitLab
- Motivation
- Time

---

#### What are Silkaj project goals?
.center[<img src="../images/silkaj_logo.svg" width=100 />]
--

- Project is aiming at creating a generic tool to manage accounts and wallets, and to monitor the currency
- Features
  - Web of Trust exploration and handling
  - Money aspects handling
  - Blockchain exploration
- Advanced users
- Minimalist tool, with advanced features

---

#### <img src="../images/Python-logo-notext.svg" width=25 /> Python

Non-steep learning curve language 

---

#### Planning
- Milestones
- Direction of the project is relatively clear
- Mentoring

---

#### Set up the development environment
##### <img src="../images/poetry-logo.svg" width=20 /> Poetry

###### Usage
- Isolated enviroment to not mess-up your system
- Handle Python dependencies
- Publish releases to PyPI

---

##### <img src="../images/poetry-logo.svg" width=20 /> Poetry
###### Installation
- [Documentation to install Silkaj with Poetry](https://git.duniter.org/clients/python/silkaj/blob/main/doc/install_poetry.md)
--

- On Debian Bullseye:
```bash
sudo apt install libsodium23 python3-pip python3-venv
python3 -m pip install poetry --user
```
--
```bash
git clone https://git.duniter.org/clients/python/silkaj.git
cd silkaj
poetry install
```
--
```bash
poetry run silkaj
```

---
##### <img src="../images/pre-commit_logo.svg" width=30 /> Pre-commit

```bash
sudo apt install pre-commit
cd silkaj
pre-commit install
```
```bash
pre-commit run --all-files
check python ast.........................................................Passed
check for merge conflicts................................................Passed
check toml...............................................................Passed
debug statements (python)................................................Passed
fix end of files.........................................................Passed
mixed line ending........................................................Passed
trim trailing whitespace.................................................Passed
black....................................................................Passed
isort....................................................................Passed
pyupgrade................................................................Passed
.gitlab-ci.yml linter....................................................Passed
Insert license in comments...............................................Passed
mdformat.................................................................Passed
```

```bash
pre-commit run --all-files black
```

---

##### <img src="../images/pre-commit_logo.svg" width=30 /> Pre-commit hooks
- `black`: Code formatting
- `isort`: Imports sorting
- `pylint`: Code lint 
- `mypy`: Static type annotation check
- `flake8`: Code checker
- `pyupgrade`: Code upgrade among Python versions

---

#### <img src="../images/pytest_logo.svg" width=50 /> Pytest

```bash
poetry run pytest
Test session starts (platform: linux, Python 3.10.4, pytest 7.1.2, pytest-sugar 0.9.4)
plugins: sugar-0.9.4, asyncio-0.10.0, cov-3.0.0
collecting ... 
 tests/test_auth.py ✓✓✓✓                              2% ▎         
 tests/test_checksum.py ✓✓✓✓✓✓✓                       4% ▌         
 tests/test_cli.py ✓                                  5% ▌         
 tests/test_crypto_tools.py ✓✓✓✓✓✓✓✓✓✓✓✓              9% ▉         
 tests/test_end_to_end.py ✓✓✓✓                       11% █▏        
 tests/test_idty_tools.py ✓✓✓✓✓✓✓✓✓✓                 14% █▌        
 tests/test_license.py ✓✓✓✓✓✓✓✓✓✓✓                   18% █▉        
 tests/test_membership.py ✓✓✓✓✓✓✓                    21% ██▎       
 tests/test_money.py ✓✓                              22% ██▎       
 tests/test_network_tools.py ✓✓✓✓✓✓✓✓✓✓✓✓            26% ██▋       
 tests/test_revocation.py ✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓ 49% ████▉     
 tests/test_tui.py ✓✓✓✓                              50% █████     
 tests/test_tx.py ✓✓✓✓✓✓✓                            53% █████▍    
 tests/test_tx_file.py ✓✓✓✓✓✓✓                       55% █████▋    
 tests/test_tx_history.py ✓✓✓✓✓✓✓✓✓✓✓✓✓✓             61% ██████▏   
 tests/test_unit_tx.py ✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓ 91% █████████▏
 tests/test_verify_blocks.py ✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓      98% █████████▊
 tests/test_wot.py ✓✓✓✓✓✓                           100% ██████████

Results (23.26s):
     265 passed
```

---
#### <img src="../images/gitlab-icon.png" width=30 /> GitLab CI/CD

GitLab-CI is running:
- `pre-commit` hooks
- Pytest on all the supported Python versions
- Build and distribute Docker images of Silkaj
- Release Python package on PyPI

---

#### Future developments
##### <img src="../images/substrate_logo.png" width=50 /> Substrate APIs usage
--

- Currently uses DuniterPy with BMA client API
- Migrate to Substrate APIs (RPC) using [`substrate-interface`](https://github.com/polkascan/py-substrate-interface) library


---

##### Graphical interface
Currently uses a (**TUI**) terminal user interface

- [Click](https://click.palletsprojects.com/): Command line interface creation kit
- Display tables with `texttable`
--

###### <img src="../images/gtk_logo.svg" width=30 /> GTK+3/4 PyGObject
--

###### `ncurses` with [`blessed`](https://github.com/jquast/blessed) library

---

name: links
#### Links
--

##### Communication means
- Technical Forum: [forum.duniter.org](https://forum.duniter.org)
- XMPP chatroom [chat.duniter.org](https://chat.duniter.org)
    - [xmpp:duniter@muc.duniter.org](xmpp:duniter@muc.duniter.org)
- Matrix chatroom: `#duniter:matrix.org`
--


##### Silkaj
- Silkaj website: https://silkaj.duniter.org/
- Silkaj repository: https://git.duniter.org/clients/python/silkaj
- [CONTRIBUTING.md](https://git.duniter.org/clients/python/silkaj/blob/main/CONTRIBUTING.md)

---

#### Credit
- Slides published under the GNU GPL v3 free license

- Slides source: [git.duniter.org/moul/slides](https://git.duniter.org/moul/slides)
- Slides Pages: [moul.duniter.io/slides/contributing_to_silkaj](https://moul.duniter.io/slides/contributing_to_silkaj/)
- Slideshow created using [remark](https://github.com/gnab/remark)
--


#### Donation
- My public key: GfKERHnJTYzKhKUma5h1uWhetbA8yHKymhVH2raf2aCP:J1k

- Or, as a QRcode:
<img src="../images/qrcode.png" width=100 />
???
If you did appreciate the presentation, you can donate here

---
class: center, middle

## Thanks
???
Thank you for your attention

---

class: center, middle
## Questions?
???
If you have a question, this is the right moment

---

name: end
class: center, middle
.center[<img src="../images/g1_logo.svg" width=500 />]
