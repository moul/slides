# Moul’s Slides

This repository contains the slides of Moul’s presentations and conferences about libre currency.

- Made with [Remark](https://github.com/gnab/remark)

## [RML16 − L’Éstachon (Sisteron) − 28th May 2022](https://rml.creationmonetaire.info/)
- [Contributing to Silkaj](https://moul.pages.duniter.org/slides/contributing_to_silkaj/)
- [Video on TuTube](https://www.youtube.com/watch?v=jfbe97El5xc)

## [RML14 − Toulouse − 28th November 2019](https://rml14.monnaielibreoccitanie.org/)
- [How to contribute to Silkaj](https://moul.pages.duniter.org/slides/how_to_contribute_to_silkaj/)
- [Video on TuTube](https://www.youtube.com/watch?v=zzmG6BlsYos)

## [RML13 − Perpignan − 23th May 2019](http://rml13.creationmonetaire.info/)
- [How to send a transaction](https://moul.pages.duniter.org/slides/how_to_send_a_transaction/)
- [Video on TuTube](https://www.youtube.com/watch?v=5H7cYOWAkfk)


## [RML12 − Bordeaux − 22th November 2018](https://rml12.duniter.io/)
- [Silkaj evolution](https://moul.pages.duniter.org/slides/silkaj_evolution/)
- [Video on TuTube](https://www.youtube.com/watch?v=xKTm6lNRGxE)

## [RMLL 2017 − Saint-Étienne − 6th July 2017](https://duniter.org/en/duniter-rmll2017/)
- [Libre currency, libre society](https://moul.pages.duniter.org/slides/rmll2017/)
- [Video](https://rmll.ubicast.tv/videos/monnaie_libre_66842_66466/)
